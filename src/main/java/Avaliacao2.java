
import utfpr.ct.dainf.if62c.avaliacao.PoligonalFechada;
import utfpr.ct.dainf.if62c.avaliacao.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Segunda avaliação parcial 2014/2.
 * @author a1363417
 */
public class Avaliacao2 {

    public static void main(String[] args) {
        PoligonalFechada p1 = new PoligonalFechada(3);
        p1.set(0, new PontoXZ(-3, 2));
        p1.set(1, new PontoXZ(-3, 6));
        p1.set(2, new PontoXZ(0, 2));     
        
        System.out.println("Comprimento da poligonal = " );
    }    
}
