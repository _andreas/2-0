/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author a1363417
 */
public class PontoXZ extends Ponto2D{

    public PontoXZ() {
        super();
    }
    
    public PontoXZ(int x, int z){
        this.x = x;
        this.y = 0;
        this.z = z;
    }    
        
    @Override
    public String toString(){
        return String.format("%s(%f,%f)",this.getNome(),x,z);
    }
    
}
