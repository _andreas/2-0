/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author a1363417
 */
public class PontoYZ extends Ponto2D{

    public PontoYZ() {
        super();
    }
    
    public PontoYZ(int y, int z){
        this.x = 0;
        this.y = y;
        this.z = z;
    }    
        
    @Override
    public String toString(){
        return String.format("%s(%f,%f)",this.getNome(),x,z);
    }
    
}
